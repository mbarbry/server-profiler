from __future__ import division
import numpy as np
import psutil
import os
import datetime
import sys
import pickle


def getStats(logFile="/media/HDD1/.log/temp.pckl", **kw):
    """
        read the temperatures from the sensors commands
    """

    new = get_chips_temp(**kw)

    try:
        data = pickle.load( open( logFile, "rb" ))
        for key, val in new.items():
            if isinstance(val, dict):
                for k2, val2 in val.items():
                    data[key][k2].append(val2)
            else:
                data[key].append(val)
        pickle.dump( data, open( logFile, "wb" ) )
    except:
        data = {}
        for key, val in new.items():
            if isinstance(val, dict):
                data[key] = {}
                for k2, val2 in val.items():
                    data[key][k2] = [val2]
            else:
                data[key] = [val]

        pickle.dump( data, open( logFile, "wb" ) )

def get_chips_temp(**kw):
    """
    get the chips temperature
    """

    fahrenheit =  kw['fahrenheit'] if 'fahrenheit' in kw else False
    interval =  kw['interval'] if 'interval' in kw else 1
    percpu =  kw['percpu'] if 'percpu' in kw else True
    interface = kw['interface'] if 'interface' in kw else "eth0"

    now = datetime.datetime.now()
    temp = {}
    time = {}
    cpu = {}

    temperature = psutil.sensors_temperatures(fahrenheit=fahrenheit)
    temp = {}

    for key, val in temperature.items():
        for te in val:
            temp[key + te[0]] = te[1]

    N = 10
    cpu_per = np.array(psutil.cpu_percent(interval=interval, percpu=percpu))
    network_per = np.array([psutil.net_io_counters(pernic=True)[interface][0],
                            psutil.net_io_counters(pernic=True)[interface][1]])
    for i in range(N):
        cpu_per += np.array(psutil.cpu_percent(interval=interval, percpu=percpu))
        network_per += np.array([psutil.net_io_counters(pernic=True)[interface][0],
                                 psutil.net_io_counters(pernic=True)[interface][1]])

    cpu_per = cpu_per/(N+1)
    network_per = network_per/(N+1)
    mem = psutil.virtual_memory()
    memory = {"available": mem[1]/(1024 * 1024),
              "used": mem[3]/(1024 * 1024),
              "free": mem[4]/(1024 * 1024)}

    network = {"bytes_sent": network_per[0],
               "bytes_recv": network_per[1]}
    cpu = {}
    for th, cp in enumerate(cpu_per):
        cpu["cpu {0}".format(th)] = cp

    return {"time": now.strftime("%Y-%m-%d %H:%M"),
            "temperature": temp,
            "cpu": cpu,
            "network": network,
            "memory": memory}


def get_chips_temp_old(cmd, log, symb = "°C"):
    """
    get the chips temperature
    """

    subprocess.call(cmd + " > " + log, shell=True)

    with open(log, "r") as f:
        lines = f.readlines()

    now = datetime.datetime.now()
    temp = {}
    time = {}
    cpu = {}

    time["time"] = np.array([now.strftime("%Y-%m-%d %H:%M")])
    for line in lines:
        if symb in line:

            interest = line.split(symb)[0]
            sep = interest.split(":")
            temp[sep[0]] = np.array([float(sep[1])])

    N = 10
    cpu_per = np.array(psutil.cpu_percent(interval=1, percpu=True))
    for i in range(N):
        cpu_per += np.array(psutil.cpu_percent(interval=1, percpu=True))

    cpu_per /= (N+1)
    system = {"cpu": cpu_per}


    return {"time": pd.DataFrame.from_dict(time),
            "temp": pd.DataFrame.from_dict(temp),
            "system": pd.DataFrame.from_dict(system)}

#getStats(logFile="temp.pckl", interface="wlp5s0")
getStats()
