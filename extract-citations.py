"""
    Small routines to extract the number of citation from your googl scholar profile
    it writes a bibliography of your paper into an html file.
    you need to enter the informations of your papers in the dictionnary papers.
"""

def get_citations_paper(titles, url):
    """
    extract the citation from your papers.

    Inputs:
        titles: list of strings containing the title of your papers
        url: string giving the url of your google scholar profile

    Outputs:
        papers: dictionnary associating the paper title to its number of citations
    """
    import urllib.request

    page = urllib.request.urlopen(url)
    page_str = page.read()
    paper_class = 'class="gsc_a_at"'
    citation_class = 'class="gsc_a_ac gs_ibl"'

    div_papers = str(page_str).split(paper_class)
    papers = {}
    for i in range(1, len(div_papers)):
        title = div_papers[i].split("</a>")[0][1:]
        if title in titles:
            citations = div_papers[i].split(citation_class)[1].split("</a>")[0]
            if len(citations) > 1:
                papers[title] = int(citations[1:])
            else:
                papers[title] = 0
    return papers

def get_write_publication_html(paper):
    """
    write the html bibliography, you will need the biblio.css file for the style
    """
    text = """
            <div class="publication">
        """
    text += '<a class="title" href="' + paper["pdf"] + '">' + paper["title"] + "</a>\n"
    text += '<p class="authors">' + paper["authors"] + '</p>\n'
    text += '<p class="details">\n      <a class="journal" href="' + paper["url"] + '">' + paper['journal'] + "</a>\n"
    text += '       ({0}), {1}({2}):'.format(paper["year"], paper["volume"], paper["number"]) + paper["pages"] + "</p>\n"
    text += 'Citations (Google Scholar): <a href="' + paper["url citations"] + '">{0}</a>'.format(paper["citations"])
    text +="""
       </div>
        
        <hr class="sep">
    """

    return text



template = """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<head>
  <title>biblio</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="generator" content="bibtex2html">
  <link rel="stylesheet" type="text/css" href="biblio.css"/>
</head>

<body>

    <div class="bibliography">
"""


closing = """
    </div>

</body>
</html>
"""

# The following lines need to be adapted to your publications

url = "https://scholar.google.com/citations?user=_r3_C4QAAAAJ&hl=fr"
titles = [
          "Efficiency loss processes in hyperfluorescent OLEDs: A kinetic Monte Carlo study",
          "PySCF-NAO: an efficient and flexible implementation of linear response time-dependent density functional theory with numerical atomic orbitals",
          "Atomic-Scale Lightning Rod Effect in Plasmonic Picocavities: A Classical View to a Quantum Effect",
          "Plasmonic response of metallic nanojunctions driven by single atom motion: quantum transport revealed in optics",
          "Atomistic near-field nanoplasmonics: reaching atomic-scale resolution in nanooptics", 
          ]


papers = {"Atomistic near-field nanoplasmonics: reaching atomic-scale resolution in nanooptics": 
            {"authors": "M.&nbsp;Barbry, P.&nbsp;Koval, F.&nbsp;Marchesin, R.&nbsp;Esteban, A.&nbsp;G. Borisov, J.&nbsp;Aizpurua, and D.&nbsp;Sánchez-Portal",
             "year":    2015,
             "volume":  15,
             "number":  5,
             "journal": "Nano Letters",
             "pages": "3410-3419",
             "pdf": "acs_2Enanolett_2E5b00759.pdf",
             "url citations": "https://scholar.google.com/scholar?oi=bibs&hl=fr&cites=14897323614236523769",
             "url": "http://dx.doi.org/10.1021/acs.nanolett.5b00759"},
          "Plasmonic response of metallic nanojunctions driven by single atom motion: quantum transport revealed in optics": 
            {"authors": "Federico Marchesin, Peter Koval, Marc Barbry, Javier Aizpurua, and Daniel Sánchez-Portal",
             "year": 2016,
             "volume": 3,
             "number": 2,
             "journal": "ACS Photonics",
             "pages": "269-277",
             "pdf": "acsphotonics_2E5b00609.pdf",
             "url citations": "https://scholar.google.com/scholar?oi=bibs&hl=fr&cites=8521632832806686113",
             "url": "http://dx.doi.org/10.1021/acsphotonics.5b00609"}, 
          "Atomic-Scale Lightning Rod Effect in Plasmonic Picocavities: A Classical View to a Quantum Effect":
           {"authors": "Mattin Urbieta, Marc Barbry, Yao Zhang, Peter Koval, Daniel Sánchez-Portal, Nerea Zabala, and Javier Aizpurua",
             "year":  2018,
             "volume": 12,
             "number": 1,
             "journal": "ACS Nano",
             "pages": "585-595",
             "pdf": "ACS_Nano_nn7b07401_ASAP.pdf",
             "url citations": "https://scholar.google.com/scholar?oi=bibs&hl=en&cites=13289663071064530846",
             "url": "http://dx.doi.org/10.1021/acsnano.7b07401"},
           "PySCF-NAO: an efficient and flexible implementation of linear response time-dependent density functional theory with numerical atomic orbitals":
           {
             "authors": "Peter Koval, Marc Barbry, Daniel Sánchez-Portal",
             "year":  2019,
             "volume": 236,
             "number": "0010-4655",
             "journal": "Computer Physics Communications",
             "pages": "188 - 204",
             "pdf": "1-s2.0-S0010465518302996-main.pdf",
             "url citations": "ihttps://scholar.google.com/citations?user=_r3_C4QAAAAJ&hl=fr&oi=ao#d=gs_md_cita-d&u=%2Fcitations%3Fview_op%3Dview_citation%26hl%3Dfr%26user%3D_r3_C4QAAAAJ%26citation_for_view%3D_r3_C4QAAAAJ%3AufrVoPGSRksC%26tzom%3D-60",
             "url": "http://www.sciencedirect.com/science/article/pii/S0010465518302996"
           },
           "Efficiency loss processes in hyperfluorescent OLEDs: A kinetic Monte Carlo study":
           {
             "authors": " Stefano Gottardi, Marc Barbry, Reinder Coehoorn, and Harm van Eersel",
             "year":  2019,
             "volume": 114,
             "number": "073301",
             "journal": "Applied Physics Letter",
             "pages": "",
             "pdf": "1.5079642.pdf",
             "url citations": "https://aip.scitation.org/doi/10.1063/1.5079642",
             "url": "https://aip.scitation.org/doi/10.1063/1.5079642"
           },

}

papers_citations = get_citations_paper(titles, url)
total = 0
for title, citation in papers_citations.items():
    papers[title]["citations"] = citation
    papers[title]["title"] = title
    total += citation

template += """
    <h2> Total citations: {0}</h2>

""".format(total)

for title in titles:
    paper = papers[title]
    template += get_write_publication_html(paper)
template += closing

# write the bibliography into file
fname = "/media/HDD-ext/website/website-perso/my-website/documents/papers/biblio.html"
f = open(fname, "w")
f.write(template)
f.close()
