Python Server-Profiler
======================

Python script to profile temperature, cpu use, memory ... and generate
html graph for online view

Installation
------------

* Prerequisites
    - Python 3.x
    - Numpy 1.8.0 or higher
    - psutil
    - plotly
