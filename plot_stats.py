from __future__ import division 
import numpy as np
import pickle

import plotly.offline as py
from plotly import tools
import plotly.graph_objs as go


def plot_prof(logFile="/media/HDD1/.log/temp.pckl", folder="/media/HDD1/website/my-website/profiles/"):

    data = pickle.load( open( logFile, "rb" ))
    ylabels = {"temperature": "T (°C)",
               "cpu": "Cpu load (%)",
               "network": "byte transfert",
               "memory": "Memory (MB)"}

    for k1, label in ylabels.items():
        trace = []

        for key in data[k1].keys():
            trace.append(
                go.Scatter( x=data['time'], # assign x as the dataframe column 'x'
                            y=data[k1][key],
                            name=key,
                            mode = "lines",
                            line=dict(width=3)
                        )
                )

        layout = go.Layout(
                    title=k1,
                    xaxis=dict(title='time'),
                    yaxis=dict(title=label),
                )
        fig =  go.Figure(data=trace, layout=layout)
        figname = folder + k1 + ".html"
        py.plot(fig, filename=figname, auto_open=False)

#plot_prof(logFile="temp.pckl", folder='./')
plot_prof()
